package modulo234_pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class PantallaEjemploCiclo {
//todo lo que este definido como atributo,  campo se pueden llamar desde cualquier lado de esta misma clase
	private JFrame frame;
	private JComboBox cmbTabla;
	private JList list;
	private String[] values = new String[10];
	private JLabel lblItemSeleccionado_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaEjemploCiclo window = new PantallaEjemploCiclo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaEjemploCiclo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 519, 471);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTablasDeMultiplicar = new JLabel("Tablas de multiplicar");
		lblTablasDeMultiplicar.setForeground(Color.GREEN);
		lblTablasDeMultiplicar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblTablasDeMultiplicar.setBounds(102, 22, 248, 29);
		frame.getContentPane().add(lblTablasDeMultiplicar);
		
		cmbTabla = new JComboBox();
		//definicion y uso de arrays
		//1- es fijo
		//2- son del mismo tipo
		String [] arrayStrTablas = new String[50];
		//comien; condicion ; contador
		//arrayStrTablas[0]=1
		for(int i = 0;i<50;i++){
			arrayStrTablas[i] = Integer.toString(i+1);
		}
		cmbTabla.setModel(new DefaultComboBoxModel(arrayStrTablas));
		cmbTabla.setFont(new Font("Tahoma", Font.BOLD, 16));
		cmbTabla.setBounds(244, 86, 92, 22);
		frame.getContentPane().add(cmbTabla);
		
		JLabel lblTabla = new JLabel("Tabla");
		lblTabla.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTabla.setBounds(170, 85, 46, 20);
		frame.getContentPane().add(lblTabla);
		
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				
				System.out.println("value changed "  + values[list.getSelectedIndex()]);
			}
		});
		list.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				System.out.println("key pressed");
				
			}
		});
		list.setFont(new Font("Tahoma", Font.BOLD, 16));

		list.setBounds(244, 135, 106, 233);
		frame.getContentPane().add(list);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//1- obtener el datos del  objeto
				String strTabla = (String)cmbTabla.getSelectedItem();
				
				//2- convertir
				 int tabla = Integer.parseInt(strTabla);
				 // lo unico que tengo que saber es que existe un array que se llama values:
				 
				 //corte y peque lo que hizo el plug in
				 for(int i = 0;i<10;i++){
					 values[i] = strTabla + "X" + Integer.toString(i) + "=" + (tabla*i);
				 }
				 
					list.setModel(new AbstractListModel() {
						public int getSize() {
							return values.length;
						}
						public Object getElementAt(int index) {
							return values[index];
						}
					});
				 
				System.out.println("strTabla:" + strTabla);
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnCalcular.setBounds(47, 272, 101, 29);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblItemSeleccionado = new JLabel("Item seleccionado");
		lblItemSeleccionado.setBounds(387, 185, 86, 14);
		frame.getContentPane().add(lblItemSeleccionado);
		
		lblItemSeleccionado_1 = new JLabel("");
		lblItemSeleccionado_1.setForeground(Color.BLUE);
		lblItemSeleccionado_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		lblItemSeleccionado_1.setBounds(387, 234, 86, 29);
		frame.getContentPane().add(lblItemSeleccionado_1);
	}
}
