package modulo234;

import java.util.Scanner;

public class EjemploDeCiclos {

	public static void main(String[] args) {
		System.out.println("ingrese una tabla a evaluar");
		Scanner scan = new Scanner(System.in);
		
		int tabla = scan.nextInt();
		
		System.out.println("la tabla a mostrar es " + tabla);
		// inicio; condicion; incremente i = i +1
		//la cantidad de ciclos es conocido
		for(int i=0; i<10; i++ ){
			int result = tabla * i;
			System.out.println(tabla + "x" + i + "=" + result);
		}
		
		//voy a ingresar valors hasta el primer numero par
		System.out.println("ingrese un numero si es par se corta");
		
		int numero = scan.nextInt();
		int resto = numero %2;
		while (resto == 1){
			System.out.println("ingrese un numero si es par se corta");
			
			numero = scan.nextInt();
			resto = numero %2;
	
		}

	}

}
