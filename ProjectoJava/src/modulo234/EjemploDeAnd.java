package modulo234;

import java.util.Scanner;

public class EjemploDeAnd {

	public static void main(String[] args) {
		//el objetivi es mostrar el mayor
		System.out.println("ingrese tres numeros enteros");

		Scanner scan = new Scanner(System.in);
		
		int a = scan.nextInt();
		int b = scan.nextInt();
		int c = scan.nextInt();
		
		if(a>b && a>c)
			System.out.println("la mayor es " + a);
		else if (b>a && b>c)
			System.out.println("la mayor es " + b);
		else if (c>a && c>b)
			System.out.println("la mayor es " + c);
		else
			System.out.println("son iguales");
	}

}
